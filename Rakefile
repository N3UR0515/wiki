desc 'Pulls down the HA-Wiki, Docker-Wiki and OMV-Wiki git repos and merges the content of their directories into the nanoc site'
task :pull_repos do
  require 'yaml'

  # By default won't delete any directories, requires all relevant directories
  # be empty. Run `RAKE_FORCE_DELETE=true rake pull_repos` to have directories
  # deleted.
  force_delete = ENV['RAKE_FORCE_DELETE']

  # Parse the config file and create a hash.
  config = YAML.load_file('./nanoc.yaml')

  # Pull guides data from the config.
  #wiki = config["guides"]["wiki"]
  hassio = config["guides"]["hassio"]
  docker = config["guides"]["docker"]
  omv = config["guides"]["omv"]

  guides = [hassio, docker, omv]
  dirs = []
  guides.each do |guide|
    dirs.push(guide['dirs']['temp_dir'])
    dirs.push(guide['dirs']['dest_dir'])
  end

  if force_delete
    puts "WARNING: Are you sure you want to remove #{dirs.join(', ')}? [y/n]"
    exit unless STDIN.gets.index(/y/i) == 0

    dirs.each do |dir|
      puts "\n=> Deleting #{dir} if it exists\n"
      FileUtils.rm_r("#{dir}") if File.exist?("#{dir}")
    end
  else
    puts "NOTE: The following directories must be empty otherwise this task " +
      "will fail:\n#{dirs.join(', ')}"
    puts "If you want to force-delete the `tmp/` and `content/` folders so \n" +
      "the task will run without manual intervention, run \n" +
      "`RAKE_FORCE_DELETE=true rake pull_repos`."
  end

  dirs.each do |dir|
    unless "#{dir}".start_with?("tmp")
  
      puts "\n=> Making an empty #{dir}"
      FileUtils.mkdir("#{dir}") unless File.exist?("#{dir}")
    end
  end

  guides.each do |guide|
    temp_dir = File.join(guide['dirs']['temp_dir'])
    puts "\n=> Cloning #{guide['repo']} into #{temp_dir}\n"

    `git clone #{guide['repo']} #{temp_dir} --depth 1 --branch master`
    
    temp_doc_dir = File.join(guide['dirs']['temp_dir'], guide['dirs']['doc_dir'], '.')
    destination_dir = File.join(guide['dirs']['dest_dir'])
    puts "\n=> Copying #{temp_doc_dir} into #{destination_dir}\n"
    FileUtils.cp_r(temp_doc_dir, destination_dir)
  end
end
